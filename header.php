<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-title" content="<?php bloginfo('name'); ?> - <?php bloginfo('description'); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div class="hfeed site" id="page" >
		<div id="site-header" <?php echo $dataBackground; ?>>
			<!-- ******************* The Navbar Area ******************* -->
					<a class="skip-link screen-reader-text sr-only" href="#content"><?php _e( 'Skip to content', 'understrap' ); ?></a>
					<nav class="navbar navbar-toggleable-sm d-flex flex-column wrapper-navbar"  itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement" id="wrapper-navbar">
						<div class="navbar-header">
							<!-- .navbar-toggle is used as the toggle for collapsed navbar content -->
							<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target=".exCollapsingNavbar" aria-controls="exCollapsingNavbar" aria-expanded="false" aria-label="Toggle navigation">MENU</i></button>

							<!-- Your site title as branding in the menu -->
							<?php if (!has_custom_logo()) { ?>
							<a class="navbar-brand text-xs-center d-inline-block" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
								<?php bloginfo( 'name' ); ?>
							</a>
							<?php } else { the_custom_logo(); } ?><!-- end custom logo -->
						</div>
					<div class="d-flex flex-column ">
						<div class="collapse navbar-collapse exCollapsingNavbar">

							<!-- Main Menu -->
							<?php wp_nav_menu(
								array(
									'theme_location' => 'primary',
									'container_class' => 'hidden-sm-down',
									'menu_class' => 'nav navbar-nav text-uppercase',
									'fallback_cb' => '',
									'menu_id' => 'main-menu',
									'walker' => new wp_bootstrap_navwalker()
									)
								);
							?>
							<!-- Top Menu -->
							<div class="utility-bar ">
								<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target=".exCollapsingNavbar" aria-controls="exCollapsingNavbar" aria-expanded="false" aria-label="Toggle navigation">CLOSE</button>
								<?php  //Curreny Converter ?>
										<div class="currency-chooser"><?php echo do_shortcode('[woocs show_flags=1 width="100px" flag_position="right" txt_type="code"]'); ?></div>

								<?php wp_nav_menu(
									array(
										'theme_location' => 'utility',
										'container_class' => 'utility',
										'menu_class' => 'nav navbar-nav link-inverse',
										'fallback_cb' => '',
										'menu_id' => 'utility-menu',
										'walker' => new wp_bootstrap_navwalker()
										)
										); ?>

							</div><!-- end .utility-bar -->
						</div>

						</div> <!-- .container -->
				</nav><!-- .site-navigation -->				
				<?php  get_template_part('partials/page', 'home-content'); ?>			
		</div><!-- .site-header end -->






					<?php  get_template_part( 'partials/ladybonnd-page-logo'); ?>
