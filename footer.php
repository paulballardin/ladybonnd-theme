<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

 $the_theme = wp_get_theme();
?>

    <?php get_sidebar('footerfull'); ?>

    <div class="wrapper" id="wrapper-footer">

      <div class="container">

        <div class="row">

          <div class="col-md-12">

            <footer class="site-footer" id="colophon" role="contentinfo">

              <div class="site-info row">
                <div class="footer-col col-md">
                  <div class="row">
                    <div class="col-md">
                     <?php wp_nav_menu(
                      array(
                        'theme_location' => 'footer_1',
                        'container_class' => 'footer',
                        'menu_class' => 'nav navbar-nav d-block text-center text-md-left',
                        'fallback_cb' => '',
                        'menu_id' => 'footer-menu-1'
                      )
                    ); ?>
                    </div>
                    <div class="col-md">
                     <?php wp_nav_menu(
                      array(
                        'theme_location' => 'footer_2',
                        'container_class' => 'footer',
                        'menu_class' => 'nav navbar-nav text-center text-md-left',
                        'fallback_cb' => '',
                        'menu_id' => 'footer-menu-2'
                      )
                    ); ?>
                    </div>
                  </div>

                </div>
                <div class="footer-col  social col-md align-middle" >
                <?php if(get_field('facebook_icon', 'option')){
                          $facebookIcon = get_field('facebook_icon', 'option');
                          $facebookIconUrl = $facebookIcon['sizes']['social-icon'];
                          $facebookIconAlt = $facebookIcon['alt'];
                          $facebookUrl = get_field('facebook_url', 'option');
                      }
                      if(get_field('instagram_icon', 'option')){
                          $instagramIcon = get_field('instagram_icon', 'option');
                          $instagramIconUrl = $instagramIcon['sizes']['social-icon'];
                          $instagramIconAlt = $instagramIcon['alt'];
                          $instagramUrl = get_field('instagram_url', 'option');
                      }
                      if ($facebookIcon) {
                       ?>
                       <?php if ($facebookUrl) {?>
                          <a href="<?php _e($facebookUrl); ?>">
                          <?php } ?>
                            <img src="<?php _e($facebookIconUrl) ?>" alt="<?php _e($facebookIconUrl) ?>" class="" >
                          <?php } ?>
                        <?php if ($facebookUrl) {?>
                        </a>
                       <?php } ?>
                       <?php if ($instagramUrl){ ?>
                          <a href="<?php _e($instagramUrl); ?>">
                      <?php } ?>
                      <?php if ($instagramIcon) { ?>
                        <img src="<?php _e($instagramIconUrl) ?>" alt="<?php _e($instagramIconUrl) ?>" class="" >
                      <?php } ?>
                      <?php if ($instagramUrl) {?>
                          </a>
                      <?php } ?>

                </div>

                <div class="footer-col form-col col-md">
                  <?php
                      $footer_subscribe_text = get_field('footer_subscribe_text', 'option');
                      if($footer_subscribe_text):
                  ?>
                    <div class="row">
                      <div class="col">
                        <h2 ><?php _e($footer_subscribe_text); ?></h2>
                        
                            <?php /* <input class="input-email" type="email" placeholder="Enter email address"><button type="submit"><i class="fa fa-chevron-right fa-2x" aria-hidden="true"></i></button> */ ?>
                            <!-- Begin MailChimp Signup Form -->                            
                            <form action="//ladybonnd.us14.list-manage.com/subscribe/post?u=93d1df9bcd84c2d6665ba5a6f&amp;id=aa4a1b5c11" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                               <div id="mc_embed_signup" class="subscribe-wrapper ">                             
                              <input type="email" value="" name="EMAIL" class="email input-email" id="mce-EMAIL" placeholder="Enter email address" required>
                                <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_93d1df9bcd84c2d6665ba5a6f_aa4a1b5c11" tabindex="-1" value=""></div>
                                <button type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"><i class="fa fa-chevron-right fa-2x" aria-hidden="true"></i></button>
                        </div>
                            </form>


                            <!--End mc_embed_signup-->

                        <div class="site-info">
                          <p class="small text-right">&copy; Lady Bonnd <?php echo date("Y") ?> All rights reserved</p>
                        </div><!-- .site-info -->
                      </div>
                    </div>
                  <?php endif; ?>
                </div>
              </div>


            </footer><!-- #colophon -->

          </div><!--col end -->

        </div><!-- row end -->

      </div><!-- container end -->

  <?php wp_footer(); ?>
  <?php get_template_part('partials/modal', 'search'); ?>

</div>
</body>

</html>
