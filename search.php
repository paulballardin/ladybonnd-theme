<?php
/**
 * The template for displaying search results pages.
 *
 * @package understrap
 */

 get_header();
?>
<div class="wrapper search-wrapper">

  <div class="container">

    <div class="row">

      <section class="<?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>col-md-8<?php else : ?>col-md-12<?php endif; ?> content-area" id="primary">

        <main class="site-main" id="main" role="main">
        
          <?php if ( have_posts() ) : ?>

          </php 
              add_filter('posts_groupby', 'group_by_post_type' );
              function group_by_post_type( $groupby ) {
                global $wpdb;
                if( !is_search() ) {
                  return $groupby;
                }
                $mygroupby = "{$wpdb->posts}.post_type";
                if( !strlen(trim($groupby))) {
                  // groupby was empty, use ours
                  return $mygroupby;
                }
                // wasnt empty, append ours
                return $groupby . ", " . $mygroupby;
              }

          ?>

            <header class="page-header">

              <h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'understrap' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
              
            </header><!-- .page-header -->

            <?php /* Start the Loop */ ?>
            <?php 
           $last_type="";
            $typecount = 0;
              while (have_posts()){
                  the_post();
                  if ($last_type != $post->post_type){
                      $typecount = $typecount + 1;
                      if ($typecount > 1){
                          echo '</div></div>'; //close type container
                      }
                      // save the post type.
                      $last_type = $post->post_type;
                      //open type container
                      switch ($post->post_type) {
                          case 'product':
                              echo "<div class=\"product container\"><h2 class='text-center type'>Product results</h2><div class='row'>";
                              break;
                          case 'post':
                              echo "<div class=\"post container\"><h2 class='text-center type'>Post results</h2><div class='row'>";
                              break;
                          case 'page':
                              echo "<div class=\"page container\"><h2 class='text-center type'>Page results</h2><div class='row'>";
                              break;
                          
                          //add as many as you need.
                      }
                  }
                /**
                 * Run the loop for the search to output the results.
                 * If you want to overload this in a child theme then include a file
                 * called content-search.php and that will be used instead.
                 */
                get_template_part( 'loop-templates/content', 'search' );

              }
              ?>
            <?php // the_posts_navigation(); ?>

          <?php else : ?>

            <?php get_template_part( 'loop-templates/content', 'none' ); ?>

          <?php endif; ?>

        </main><!-- #main -->

      </section><!-- #primary -->

      <?php get_sidebar(); ?>

    </div><!-- .row -->

  </div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>
