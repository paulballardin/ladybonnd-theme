<?php
/**
 * Add WooCommerce support
 *
 *
 * @package understrap
 */

// Adds woococmerce Theme Suppprt
add_action( 'after_setup_theme', 'woocommerce_support' );
if ( ! function_exists ( 'woocommerce_support' ) ) {
	function woocommerce_support() {
	    add_theme_support( 'woocommerce' );
	}
}

/* Product Archive Hooks  */

//1. Remove unnessary defaults
remove_action('woocommerce_before_shop_loop', 'woocommerce_result_count',20);
remove_action('woocommerce_before_shop_loop', 'woocommerce_catalog_ordering',30);

//Remove the Title on the Shop Page
add_filter( 'woocommerce_show_page_title' , 'woo_hide_page_title' );
function woo_hide_page_title() {
	if(is_shop()){	
		return true;
	}
}

//remove the breadcrumbs
remove_action( 'woocommerce_before_main_content','woocommerce_breadcrumb', 20  );

//Remove the price on shop pages
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );

//Add action excerpt on Archive Pages
add_action( 'woocommerce_after_shop_loop_item_title','woocommerce_template_loop_excerpt', 20);
if ( ! function_exists ( 'woocommerce_template_loop_excerpt' )  ) {
	function woocommerce_template_loop_excerpt() {
		woocommerce_template_single_excerpt();
	}
}





//4. Display Products 3 columns wide
// Change number or products per row to 3
add_filter('loop_shop_columns', 'loop_columns');
if (!function_exists('loop_columns')) {
	function loop_columns() {
		return 3; // 3 products per row
	}
}

//5. Change Add to basket Text
add_filter( 'woocommerce_product_add_to_cart_text', 'woo_archive_custom_cart_button_text' );    // 2.1 +

function woo_archive_custom_cart_button_text() {

        return __( 'Buy now', 'woocommerce' );

}

//6. Add Woocomerce widget area to top of product archive page 
function os_add_woocommerce_widget() {
	get_sidebar('footerfull');
}
add_action( 'woocommerce_before_shop_loop', 'os_add_woocommerce_widget'  );


//7. Set the number of products per page
add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );

function new_loop_shop_per_page( $cols ) {
  // $cols contains the current number of products per page based on the value stored on Options -> Reading
  // Return the number of products you wanna show per page.
  $cols = 24;
  return $cols;
}
  
//8. Change the shop title
function change_woocommerce_category_page_title( $page_title )
{
	if ( is_shop() ) {
		
		$page_title = "Shop Lady Bonnd";
		
	}
	return $page_title;
}

//9. add the filter
add_filter( 'woocommerce_page_title', 'change_woocommerce_category_page_title', 10, 1 );


/* Single Product Hooks */

add_action( 'woocommerce_before_single_product_summary', 'woocommerce_start_container_div', 0 );
function woocommerce_start_container_div() {
	echo '<div class="container">';
}
add_action( 'woocommerce_after_single_product_summary', 'woocommerce_end_container_div', 99 );
function woocommerce_end_container_div() {
	echo '</div>';
}
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
remove_action('woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15);
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);


// Add related products to a stripe
add_action( 'woocommerce_upsell_product_stripe', 'woocommerce_upsell_display', 10 );
add_action( 'woocommerce_related_product_stripe', 'woocommerce_output_related_products', 10 );


/**
 * Change text strings
 *
 * @link http://codex.wordpress.org/Plugin_API/Filter_Reference/gettext
 */
function my_text_strings( $translated_text, $text, $domain ) {
	switch ( $translated_text ) {
		case 'Related products' :
			$translated_text = __( 'You may also like...', 'woocommerce' );
			break;
	}
	return $translated_text;
}
add_filter( 'gettext', 'my_text_strings', 20, 3 );


/**

 *
 * Change number of related products on product page
 * Set your own value for 'posts_per_page'
 *
 */ 
function woo_related_products_limit() {
  global $product;
	
	$args['posts_per_page'] = 3;
	return $args;
}
add_filter( 'woocommerce_output_related_products_args', 'jk_related_products_args' );
  function jk_related_products_args( $args ) {

	$args['posts_per_page'] = 3; // 4 related products
	$args['columns'] = 3; // arranged in 2 columns
	return $args;
}

// replaces the excerpt with the full description on single product pages
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );

add_action('woocommerce_single_product_summary', 'woocommerce_template_single_content', 20 );
function woocommerce_template_single_content() {
	the_content();
}

// Changes the add to cart text



?>
