<?php
/**
 * @package understrap
 */
?>

<article <?php post_class(' col-sm-4 blogPost'); ?> id="post-<?php the_ID(); ?>">

  <header class="entry-header">

    <?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>

    <?php if ( 'post' == get_post_type() ) : ?>

      <!--<div class="entry-meta">
        <?php understrap_posted_on(); ?>
      </div> .entry-meta -->

    <?php endif; ?>

  </header><!-- .entry-header -->

  <?php echo get_the_post_thumbnail( $post->ID, 'large', array( 'class' => 'img-fluid' ) ); ?>

  <div class="entry-content">
    <p>
       <?php echo aw_substr_word(wp_strip_all_tags(get_the_content()), 100).'..'; ?> 
       </p>
       <a class="read-more-link" href="<?php echo get_permalink($row->ID); ?>" class="moreLink">continue </a>
    <?php
      wp_link_pages( array(
        'before' => '<div class="page-links">' . __( 'Pages:', 'understrap' ),
        'after'  => '</div>',
      ) );
    ?>

  </div><!-- .entry-content -->

  <!--<footer class="entry-footer">

    <?php understrap_entry_footer(); ?>

  </footer> .entry-footer -->

</article><!-- #post-## -->
