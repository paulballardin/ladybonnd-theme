<?php
/**
 * @package understrap
 */
?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

  <header class="entry-header">

    <?php the_title( '<h2 class="entry-title">', '</h2>' ); ?>

    <!--<div class="entry-meta">

      <?php understrap_posted_on(); ?>

    </div> .entry-meta -->

  </header><!-- .entry-header -->

  <?php echo get_the_post_thumbnail( $post->ID, 'full', array( 'class' => 'img-fluid' ) ); ?>

  <div class="entry-content">

    <?php the_content(); ?>

      <?php 
      
      $args = array (
        'before'            => '<div class="page-links-XXX"><span class="page-link-text">' . __( 'More pages: ', 'textdomain' ) . '</span>',
        'after'             => '</div>',
        'link_before'       => '<span class="page-link">',
        'link_after'        => '</span>',
        'next_or_number'    => 'next',
        'separator'         => ' | ',
        'nextpagelink'      => __( 'Next &raquo', 'textdomain' ),
        'previouspagelink'  => __( '&laquo Previous', 'textdomain' ),
    );
        wp_link_pages( $args );
      ?>

  </div><!-- .entry-content -->

  <!--<footer class="entry-footer">

    <?php understrap_entry_footer(); ?>

  </footer> .entry-footer -->

</article><!-- #post-## -->
