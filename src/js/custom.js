//Load Typekit
try{Typekit.load({ async: true });}catch(e){}




jQuery(document).ready(function( $ ) {
	
	
	function featurette_widths(){
		$container = $('#first-container').css('width');
		$featureWidth = parseInt($container)/2;
			$('#featurette .feature').each(function() {
				if($featureWidth > 570) {  
					$(this).css('width', $featureWidth);
				} else {
					$(this).css('width', 'auto')	
				}		
			});

	}
	//featurette_widths();

	function page_banner(){
		section = $('section.page-banner.banner-image');
		pageBanner = $(section).data('image');
		sectionClass = $(section).data('class');
		if(pageBanner){
			$('#wrapper-navbar').delay( 3600 ).fadeIn( 3600 ).addClass('banner-image '+sectionClass).delay( 3600 ).css({
				'background-image': 'url('+pageBanner+')'
			});
		}		
	}
	page_banner();

	var carousel = $(".wrapper-fluid");

	//Slick Slider
	function initCarousel(carousel){
		
		carousel.slick({
			autoplay: true,
			speed: 700,
			arrows: false,
			adaptiveHeight: true,
			slickPlay: true, 
			fade: true,
			pauseOnHover: true,
			pauseOnFocus: true
		})
	};
	initCarousel(carousel);

	

	


  //killer resize function
  var doit;
  function resizedw(){
     // featurette_widths()          
  }
  window.onresize = function() {
      clearTimeout(doit);
      doit = setTimeout(function() {
          resizedw();
		  carousel.slick('unslick');
		  initCarousel(carousel);
      }, 100);
  };

    // End killer resize function
    // 

	//Trigger modal
	$('.search-modal-trigger').on('click', function(){
		$('#search-modal').modal();
	})
	
});

