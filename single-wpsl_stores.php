<?php
/**
 * The template for displaying all single posts.
 *
 * @package understrap
 */

 get_header();
?>
<div class="wrapper" id="single-wrapper">

  <div class="container" id="content">

    <div class="row">

      <div class="<?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>col-md-8<?php else : ?>col-md-12<?php endif; ?> content-area" id="primary">

        <main class="site-main" id="main" role="main">
            <header class="entry-header text-center">
            <?php the_title( '<h2 class="entry-title">', '</h2>' ); ?>
            </header><!-- .entry-header -->
            <div class="row">
                
                <?php while ( have_posts() ) : the_post(); ?>
                  


                
                    <?php //get_template_part( 'loop-templates/content', 'single' ); ?>
                    <?php
                            global $post;
                            $queried_object = get_queried_object();
                            ?>
                            <div class="single-store-col col-6 col-sm-6 col-md-6 col-lg-4">                                                        
                            <?php                             
                                // Add the address shortcode
                                echo do_shortcode( '[wpsl_address]' );
                            ?>
                            </div>
                            <div class="single-store-col col-6 col-sm-6 col-md-6 col-lg-4">                            
                                <strong>Store Hours</strong>
                                <?php echo do_shortcode( '[wpsl_hours]' ); ?>
                            </div>
                            <div class="single-store-col col-12 col-sm-6 col-md-12 col-lg-4">                                                        
                            <?php 
                                // Add the map shortcode
                                echo do_shortcode( '[wpsl_map zoom="16"]' ); ?>
                            </div>
                            <?php
                            // Add the content
                            $post = get_post( $queried_object->ID );
                            setup_postdata( $post );
                            // the_content();
                            wp_reset_postdata( $post );
                            ?>
   
                            
                    
                <?php endwhile; // end of the loop. ?>
                </div>

        </main><!-- #main -->

      </div><!-- #primary -->

      <?php get_sidebar(); ?>

    </div><!-- .row -->

  </div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>
