<?php get_header(); ?>

<?php // echo do_shortcode('[rev_slider alias="blog-post-slider"]'); ?>


<div id="main" class="blogPage" role="main">
  <div id="content" class="">
    <h1 class="text-center">The Muse</h1>
    
		<?php
    # Get shortcode parameters
    extract( shortcode_atts( array('limit' => '4'), $atts ) );
    $categories = get_categories( 'type=post&child_of=0&taxonomy=category&orderby=date&order=ASC' );
    if($categories) {
      //echo count($categories);
      $cat_cnt = 1;
      ?>
        <?php
        foreach($categories as $category) {
          
          if($limit < $cat_cnt) {
            break;
          }
          
          $category_custom_class = 'default';
          if($cat_cnt % 2 != 0) {
            $category_custom_class = 'grey';
          }
          $category_url = get_category_link($category->cat_ID);
          ?>
          
          <div class=" categorySection <?php  echo $category_custom_class; ?>" >
            <div class="container">
                  <h2 class="text-center"><a href="<?php echo $category_url; ?>"><?php echo $category->name; ?></a></h2>
  
          
                <?php
                $rs = get_posts('numberposts=3&post_type=post&category='.$category->cat_ID.'&post_status=publish&orderby=post_date&order=DESC');
                if($rs) {
                    ?>
                    <div class="row">
                        <div class="col-md-12">
                        <div class="row">
                            <?php
                            foreach($rs as $row) {
                            $row_image_url_arr = wp_get_attachment_image_src( get_post_thumbnail_id($row->ID), 'thumb');
                            //print2($row_image_url_arr);
                            $row_image_url = $row_image_url_arr ? $row_image_url_arr['0']:STYLESHEET_DIR.'/images/recent-post-no-img.jpg';
                            ?>
                        
                        
                                <div class="col-xs-12 col-sm-4 col-md-4 blogPost">
                                <a href="<?php echo get_permalink($row->ID); ?>">
                                    <img src="<?php echo $row_image_url; ?>" alt="<?php echo aw_escape_text($row->post_title); ?>" title="<?php echo aw_escape_text($row->post_title); ?>" width="555" height="365" />
                                </a>
                                    
                                    <h3><a href="<?php echo get_permalink($row->ID); ?>"><?php echo $row->post_title; ?></a></h3>
                                    <p> 
                                    <?php echo aw_substr_word(wp_strip_all_tags($row->post_content), 100).'..'; ?> 
                                    </p>
                                    <a class="read-more-link" href="<?php echo get_permalink($row->ID); ?>" class="moreLink">continue </a>
                                </div>
                            
                            <?php
                            }
                            ?>
                        </div>
                        </div>
                    </div>
                    </div>
                    <?php
            }
            ?>
            </div>
          <?php
          $cat_cnt++;
        }
        ?>
        
      <?php
    } 
    ?>

	
    </div>

</div>

<?php get_footer(); ?>
