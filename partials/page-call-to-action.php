<?php
$cta_title = get_sub_field('cta_title');
if (!empty($cta_title)) {
$cta_title = get_sub_field('cta_title');

$cta_text = get_sub_field('cta_text');
if (empty($cta_text)) {
	$cta_text = '';
}
$cta_background = get_sub_field('cta_background');
if (empty($cta_background)) {
	$cta_background = '';
}
?>

<section class="stripe call-to-action text-center <?php _e($cta_background); ?>" >
	<div class="container">
		<div class="row">
			<div class="cta-wrapper table">
				<?php if($cta_title): ?>
					<h2><?php _e($cta_title); ?></h2>
				<?php endif; ?>
				<div class="cta-text"><?php _e($cta_text); ?></div>

				<?php if( have_rows('cta_buttons') ): ?>
					<div class="cta-button-wrapper row flex-items-xs-center">
						<?php  while ( have_rows('cta_buttons') ) : the_row();
							$cta_button_label = get_sub_field('cta_label');
							if(empty($cta_button_label)): $cta_button_label = 'find out more'; endif;
							$cta_link = get_sub_field('cta_link');
							if (!empty($cta_link)):
						?>
							<div class="col-md-5 col-lg-4 col-xl-3 col-xs-10 col-sm-10 ">
								<a href="<?php _e($cta_link); ?>" class="btn btn-primary btn-block btn-lower " ><?php _e($cta_button_label); ?></a>
							</div>
							<?php endif; ?>
						<?php endwhile; ?>
				</div>
			<?php endif // end buttons ?>

			</div>
		</div>

	</div>
</section>

<?php } ?>