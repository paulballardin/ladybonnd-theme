<?php


$banner_image = get_field('page_header_banner', 'option');
$banner_image_url = $banner_image['sizes']['page-header'];
$style =  sprintf('style="background-image:url(%s);"', $banner_image_url);

?>

<section class="stripe default">
  <div class="container" id="content">
  
        <main class="site-main" id="main" role="main">

          <?php while ( have_posts() ) : the_post(); ?>
              <?php get_template_part( 'loop-templates/content', 'page' ); ?>
          <?php
              // If comments are open or we have at least one comment, load up the comment template
              if ( comments_open() || get_comments_number() ) :
                  comments_template();
              endif;
            ?>

          <?php endwhile; // end of the loop. ?>

        </main><!-- #main -->

  </div><!-- Container end -->
</section>


