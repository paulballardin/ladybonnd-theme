<div id="search-modal" class="modal fade">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header ">
        <!--<h5 class="modal-title">Explore Lady Bonnd</h5>-->
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <?php get_template_part('searchform'); ?>
      </div>
      <!--<div class="modal-footer">
        <h3>Something here</h3>
      </div>-->
    </div>
  </div>
</div>