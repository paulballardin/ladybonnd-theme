<?php
/**
 *
 * This template controls the additionallstripes on the woocmmerce single product page
 *
 *  Todo: add conditional logic for showing  / not showing the stripes
 *
 */


?>

<?php get_template_part( 'partials/ladybonnd-product-page-logo'); ?>
<?php if(have_rows('product_page_features_list')) { ?>
<section id="lady-bonnd-features" class="product-page-features">
	<div class="container">
		<div class="row">
			<?php  while ( have_rows('product_page_features_list') ) : the_row(); ?>
				<div class='col-md feature'>
				<?php
					if(get_sub_field('image')) {
						$image = get_sub_field('image');
						$url = $image['url'];
						$alt = $image['alt'];
					?>
					<p class="text-xs-center"><img src="<?php _e($url); ?>" alt="<?php _e($alt); ?>"></p>
				<?php } ?>
				<?php
					if(get_sub_field('title')) {
					$title = get_sub_field('title');
				?>
					<h3 class="text-xs-center"><?php _e($title); ?></h3>
				<?php } ?>
				<?php
					if(get_sub_field('text')) {
				?>
					<?php the_sub_field('text'); ?>
				<?php } ?>
				</div>
			<?php endwhile; ?>
		</div>
	</div>
</section>


<?php } // end has rows  product_page_features_list ?>
<section id="related-products" class="related-products-section">
		<div class="container">
			<?php
				global $product;
				$upsells = $product->get_upsells();
				if ( sizeof($upsells) == 0 ){
					do_action('woocommerce_related_product_stripe');
				} else {
					do_action('woocommerce_upsell_product_stripe');
				}
			?>

		</div>
</section>
<?php 
 // Check if there are featurettes

if( have_rows('layouts', 'option') ):
get_template_part('partials/page', 'stripe');
endif;

?>
