<?php
$background = get_sub_field('background');
if (empty($background)) {
	$background = 'white';
}
$text_align = get_sub_field('text_align');
if (empty($text_align)) {
	$text_align = 'text-center';
}  
?>
<section class="additional-content <?php echo $text_align;?> <?php echo $background;  ?>" ?>
		<div class="additional-content-content container">
			 <?php				
					echo get_sub_field('content');
			 ?>
		</div>
</section>