<?php
$title = get_sub_field('title');
$content = get_sub_field('content');
$image = get_sub_field('image');
$imageAlign = get_sub_field('image_align');
?>
<?php if($title || $content || $image){ ?>



<section class="image-content-block" >
	<div class="container">
		<div class="row flex-items-md-middle">
			<?php if($image) { ?>
				<div class="col-md image-wrapper text-center <?php if($imageAlign): _e($imageAlign); endif; ?>">
					<?php if($image):_e('<img src="'.$image['url'].'" alt="'.$image['alt'].'" class="img-fluid">');endif; ?></h3>
				</div>
			<?php } ?>
			<?php if($title || $content) { ?>
				<div class="col-md content-wrapper ">
					<?php if($title):_e('<h3>'.$title.'<h3>');endif; ?></h3>
					<?php if($content){ ?>
						<div class="content">
							<?php _e($content); ?>
						</div>
					<?php } ?>
				</div>
			<?php } ?>

		</div>
	</div>
</section>

<?php } ?>