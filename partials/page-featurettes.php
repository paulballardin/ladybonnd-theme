


<section id="featurette" class="stripe featurettes" <?php if (!empty($banner_image)){ echo $style;} ?>>
	<?php if( have_rows('featurettes') ){ ?>	
		<div class="features-row row m-0" >
		<?php $counter =1; ?>
		<?php while ( have_rows('featurettes') ) : the_row(); ?>
			<?php
				$feature_background = get_sub_field('feature_background');
				$bgstyle = '';
				if(!empty($feature_background)){
					$feature_background_url = $feature_background['url'];
					$bgstyle =  sprintf('style="background-image:url(%s);"', $feature_background_url);
				}
				$feature_image = get_sub_field('feature_image');
				$style = '';
				if(!empty($feature_image)){
					$feature_image_url = $feature_image['url'];
					$style =  sprintf('style="background-image:url(%s);"', $feature_image_url);

				}
				$featureId = get_field('feature_id');
				?>
			<div <?php if (!empty($featureId)): echo $featureId; endif; ?> class="feature-wrapper col-lg-12 p-0" <?php echo $bgstyle; ?> >

				<div class="feature d-flex justify-content-around" <?php echo $style; ?>>
					<div class="feature-content text-center col-sm-6 col-md-3">
						<?php
							$feature_title = get_sub_field('feature_title');
							$feature_subtitle = get_sub_field('feature_subtitle');
							$feature_content = get_sub_field('feature_content');
							$feature_button_url = get_sub_field('feature_button_url');
							$feature_button_label = get_sub_field('feature_button_label');
						?>
					<div class="feature-content-wrapper">
					<?php if(!empty($feature_title)) { ?>
						<h2 class=""><?php _e($feature_title); ?></h2>
					<?php } ?>
					<?php if(!empty($feature_title)) { ?>
						<h4 class=""><?php _e($feature_subtitle); ?></h4>
					<?php } ?>


					<?php if(!empty($feature_content)) { ?>
						<?php _e($feature_content); ?>
					<?php } ?>
					<?php if(!empty($feature_button_url)) { ?>
					<a class="btn btn-primary " href="<?php _e($feature_button_url); ?>"><?php if(!empty($feature_button_label)) { _e($feature_button_label); } else { _e('Learn More'); }?></a>
					<?php } ?>
						</div>

					</div>
				</div>
			</div>
		<?php $counter ++; ?>
		<?php endwhile; ?>
		</div>
	<?php } ?>
</section>