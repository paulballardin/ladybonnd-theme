<?php 


if(is_home()) {
	$page_id = get_option( 'page_for_posts' ); 
} elseif(is_shop()) {
	$page_id = get_option( 'woocommerce_shop_page_id' ); 
}elseif(is_product()) {
	$page_id = 'option';
	
}
 else {
	$page_id = get_the_ID();
}

if( have_rows('layouts', $page_id) ):
	
    while ( have_rows('layouts', $page_id) ) : the_row();
		
		//Page Content
			    
		if( get_row_layout() == 'content' ):
			get_template_part('partials/page', 'content');
		endif;
		

		//Page Banners	    
	    if( get_row_layout() == 'banner' ):
	    	get_template_part('partials/page', 'banner');
	    endif;

	    //Call to action 
	    if( get_row_layout() == 'call_to_action' ):
	    	get_template_part('partials/page', 'call-to-action');
	    endif;

	    //Additional Content   
	    if( get_row_layout() == 'additional_content' ):
	    	get_template_part('partials/page', 'additional-content');
	    endif;

	    //Feature boxes    
	    if( get_row_layout() == 'feature_boxes' ):
	    	get_template_part('partials/page', 'feature-boxes');
	    endif;

	    //Feature tiles    
	    if( get_row_layout() == 'feature_tiles' ):
	    	get_template_part('partials/page', 'feature-tiles');
	    endif;

	    //Featurettes    
	    if( get_row_layout() == 'featurettes' ):
	    	get_template_part('partials/page', 'featurettes');
	    endif;

	    //Featurettes    
	    if( get_row_layout() == 'image_content_block' ):
	    	get_template_part('partials/page', 'image-content-block');
	    endif;

	   	//Testimonials    
	    if( get_row_layout() == 'testimonials' ):
	    	get_template_part('partials/page', 'testimonials');
	    endif;

	    //Woocommerce    
     	if( get_row_layout() == 'woocommerce_section' ):
     		get_template_part('partials/page', 'woocommerce-section');
     	endif;

	    
	endwhile;
endif;	
if (!is_shop() && !is_product() ):
	if (is_archive() || is_single()){
		if (!is_archive()){
		get_template_part('partials/page', 'content');
		}
		get_template_part('partials/page', 'call-to-action');
		get_template_part('partials/page', 'testimonials');
	}
endif;

 ?>