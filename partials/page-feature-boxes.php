<?php

$section_heading = get_sub_field('section_heading');
$section_subheading = get_sub_field('feature_subheading');
$section_content = get_sub_field('section_content');
?>


<section class="stripe feature-boxes" <?php if (!empty($banner_image)){ echo $style;} ?>>
	<div class="container">
	<?php if(!empty($section_heading) || !empty($section_subheading) || !empty($section_content)) { ?>
		<div class="feature-intro">
	<?php } ?>
	<?php if (!empty($section_heading)) {?>
		<h2 class="text-center"><?php echo $section_heading; ?></h2>
	<?php } ?>
	<?php if (!empty($section_subheading)) {?>
		<h4 class="text-center"><?php echo $section_subheading; ?></h4>
	<?php } ?>

	<?php if (!empty($section_content)) {?>
	<div class="text-center">
		<?php echo $section_content; ?>
	</div>
	<?php } ?>
	<?php if(!empty($section_heading) || !empty($section_subheading) || !empty($section_content)) { ?>
		</div>
	<?php } ?>

	<?php if( have_rows('features') ){ ?>
		<div class="features-row row " >
		<?php while ( have_rows('features') ) : the_row(); ?>

			<div class="feature-wrapper col-sm-10 offset-sm-1 col-md-4 offset-md-0">
				<div class="feature ">
					<div class="feature-content"><?php

					$feature_title = get_sub_field('feature_title');
					$feature_content = get_sub_field('feature_content');
					$feature_button_url = get_sub_field('feature_button_url');
					$feature_button_label = get_sub_field('feature_button_label');
					$feature_icon = get_sub_field('feature_icon');

					?>
					<?php if(!empty($feature_icon)){ ?>
					<div class="float-md-left text-center text-md-left">
						<img class="" src="<?php _e($feature_icon['sizes']['thumbnail']);?>" alt="<?php _e($feature_icon['alt']);?>">
					</div>
					<?php } ?>

					<div class="media-body>">
					<?php if(!empty($feature_title)) { ?>
						<h3 class="media-heading"> <?php _e($feature_title); ?></h3>
					<?php } ?>


					<?php if(!empty($feature_content)) { ?>
						<p><?php _e($feature_content); ?></p>
					<?php } ?>
						</div>
					<?php if(!empty($feature_button_url)) { ?>
						<a class="btn btn-primary " href="<?php _e($feature_button_url); ?>"><?php if(!empty($feature_button_label)) { _e($feature_button_label); } else { _e('Learn More'); }?></a>
					<?php } ?>
					</div>
				</div>
			</div>

		<?php endwhile; ?>
		</div>
	<?php } ?>
	</div>
</section>