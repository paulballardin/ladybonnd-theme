<div class="wrapper-fluid slider">
	<?php
if( have_rows('home_banners')) : ?>
<?php 	
	while ( have_rows('home_banners') ) : the_row(); 
		$dataBackground = '';
		if (get_sub_field('home_banner_background')){
			$dataBackground = get_sub_field('home_banner_background');
			$dataBackground = $dataBackground['url'];
			$dataBackground = 'style="background-image:url('.$dataBackground.');"';
		}
		$dataBanner = '';
		if (get_sub_field('home_banner')){
			$dataBanner = get_sub_field('home_banner');
			$dataBanner = $dataBanner['url'];
			$dataBanner = 'style="background-image:url('.$dataBanner.');"';
		}
$home_title = get_sub_field('home_title');
$home_blurb = get_sub_field('home_blurb');
$home_cta_link = get_sub_field('home_cta_link');
$home_cta_label = get_sub_field('home_cta_label');

if ($home_title || $home_blurb || $home_cta_link) {
	?><div class="home-feature" <?php echo $dataBackground; ?>>
		<div class="home-feature-wrapper" <?php echo $dataBanner; ?>>
			<div class="home-feature-content">
				<div class="col-md-6">
					<div class="d-flex flex-column text-center ">
							<div class="col-md-6 offset-md-3 col-sm-8 offset-sm-2">
							<?php
							if($home_title): _e('<h1 class="text-uppercase">'.$home_title.'</h1>'); endif;
							if($home_blurb): _e('<div class="text-lowercase">'.$home_blurb.'</div>'); endif;
							if($home_cta_link) {
								if(empty($home_cta_label)){
									$home_cta_label = 'Find our more';
								}

								_e('<a href="'.$home_cta_link.'" class="btn btn-primary text-uppercase" title="$home_cta_label">'.$home_cta_label.'</a>');
							}
							?>
							</div>
						</div>
					</div>
				</div>
			</div>	
				<?php /*
				if($home_title): _e('<h1 class="text-uppercase">'.$home_title.'</h1>'); endif;
				if($home_blurb): _e('<div class="text-lowercase">'.$home_blurb.'</div>'); endif;
				if($home_cta_link) {
					if(empty($home_cta_label)){
						$home_cta_label = 'Find our more';
					}

					_e('<a href="'.$home_cta_link.'" class="btn btn-primary text-uppercase" title="$home_cta_label">'.$home_cta_label.'</a>');
				}
			 */	?>
		</div>
				<?php

}
	endwhile; ?>
			</div>
		</div>
	<?php	
endif;
?>
</div><!-- .wrapper-navbar end -->
