<section class="stripe feature-tiles" <?php if (!empty($banner_image)){ echo $style;} ?>>
	<div class="container">

	<?php if( have_rows('features') ){ ?>
		<div class="features-row row " >
		<?php while ( have_rows('features') ) : the_row(); ?>
			<?php
				$featureId = '';
				if (get_sub_field('feature_id')){
					$featureId = 'id="'.get_sub_field('feature_id').'"';
				}
				$feature_button_url = get_sub_field('feature_button_url');
			?>
			<?php if(!empty($feature_button_url)) { ?>
				<a <?php _e($featureId); ?> class="feature-wrapper col-md text-center" href="<?php _e($feature_button_url); ?>">
			<?php } ?>
					<div  class="feature-content ">

					<?php
					$feature_title = get_sub_field('feature_title');
					$feature_icon = get_sub_field('feature_icon');
					?>
					<?php if(!empty($feature_icon)){ ?>
						<div class="feature-content-top">

							<img class="vertically-align" src="<?php _e($feature_icon['sizes']['thumbnail']);?>" alt="<?php _e($feature_icon['alt']);?>">
					<?php } ?>
					</div>
					<?php if(!empty($feature_title)) { ?>
						<h3><?php _e($feature_title); ?></h3>
					<?php } ?>
					</div>

			<?php if(!empty($feature_button_url)) { ?>
				</a>
			<?php } ?>

		<?php endwhile; ?>
		</div>
	<?php } ?>
	</div>
</section>