<?php 

$sub_pages = get_sub_field('subpage_ids');

//$sub_pages = implode(", ", $sub_pages);

// WP_Query arguments
$args = array (
	'post__in'                => $sub_pages,
	'post_type'              => 'page',
);

// The Query
$query = new WP_Query( $args );

// The Loop
if ( $query->have_posts() ) {
	?>
	
	<section class="stripe sub-pages"> 
		<div class="container">
			<div class="sub-pages-row row row-centered" >

	<?php 
	while ( $query->have_posts() ) {
		$query->the_post();
	?>
				<div class="sub-page-wrapper col-sm-6 equalHeight col-centered">
					<div class="row">
						<div class="col-sm-6">
							<div class="sub-page text-left">
									<h3><?php the_title(); ?></h3>
									<p class="sub-page-excerpt"><?php the_excerpt(); ?></p>
									<a href="<?php the_permalink(); ?>" class="btn btn-primary">Find out more</a>
									</div>
							</div>
						<div class="col-sm-6">
						<?php if(has_post_thumbnail()){
							the_post_thumbnail('sub-pages', array( 'class' => 'img-responsive' ));
						}?>
						</div>
					</div>
				</div>
	<?
	}
	?>
			</div>
		</div>
	</section>
	<?php
} else {
	// no posts found
}

// Restore original Post Data
wp_reset_postdata();

 ?>




