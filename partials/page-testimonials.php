<?php  // WP_Query arguments for custom post type - testimonial

$args = array (
	'post_type'              => array( 'testimonial' ),
	'orderby'                => 'rand',
	'posts_per_page' 		=> '1',
);

// The Query
$query = new WP_Query( $args );
if ( $query->have_posts() ) {
	while ( $query->have_posts() ) {
		$query->the_post();
// The Loop
		$attribution = get_sub_field('attribution');

?>
<section class="stripe testimonial text-center">
	<div class="container">
		<div class="testimonial-wrapper">
			<div class="row">
				<div class="col-md-1 offset-md-1 text-left">
					<i class="fa fa-quote-left fa-2x" aria-hidden="true"></i>
				</div>
				<div class="testimonial-content col-md-6 offset-md-1">
					<?php the_content(); ?>
					<div class="attribute-wrapper">
					<p><cite><?php echo get_post_meta( get_the_ID(), 'attribution', true ); ?></cite></p>
					</div>
				</div>
				<div class="col-md-1 offset-md-1 text-right flex-md-bottom">
					<i class="fa fa-quote-right fa-2x " aria-hidden="true"></i>
				</div>

			</div>
		</div>
	</div>
</section>
<?php
	} // Endwhile

} else {
	// no posts found
}

// Restore original Post Data
wp_reset_postdata();

?>

