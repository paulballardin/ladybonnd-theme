
<?php

$banner_image = get_sub_field('banner_image');
$banner_full_width = get_sub_field('banner_full_width');
$banner_full_width = sprintf('data-class="%s"', $banner_full_width);
$banner_image_url = $banner_image['url'];
$style =  sprintf('data-image="%s"', $banner_image_url);
$banner_title = get_sub_field('banner_title');
$banner_text = get_sub_field('banner_text');
$banner_button_link = get_sub_field('banner_button_link');
$banner_url = get_sub_field('banner_url');
$banner_button_label = get_sub_field('banner_button_label');



 ?>
<section class="page-banner  <?php if (!empty($banner_image)){ echo 'banner-image'; } ?> text-center " <?php if (!empty($banner_image)){ echo $style;} ?> <?php if($banner_full_width): _e($banner_full_width); endif; ?>>
		<div class="page-banner-content container">
			<?php if (!empty($banner_title)): ?>
				<h1><?php _e($banner_title) ?></h1>
			<?php endif; ?>

				<?php if (!empty($banner_text)): ?>
					<div class="banner-content">
						<?php _e($banner_text) ?>
					</div>
				<?php endif ?>

				<?php if (!empty($banner_button_link)): ?>
					<a class="btn btn-primary cta" href="<?php _e($banner_button_link) ?>" ><?php
						if(!empty($banner_button_label)){
							_e($banner_button_label);
						}
						else
						{
							_e("Find out more");
						}
					?></a>
				<?php endif ?>


		</div>
</section>
