<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package understrap
 */

 get_header();
?>

<?php
$container = get_theme_mod('understrap_container_type');
$sidebar_pos = get_theme_mod('understrap_sidebar_position');
?>

<div class="wrapper" id="page-wrapper">

  <?php get_template_part('partials/stripes') ?>

</div><!-- Wrapper end -->

<?php get_footer(); ?>
